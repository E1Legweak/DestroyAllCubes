﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

public class SaveDataManager : MonoBehaviour
{
    [SerializeField]
    int GoldMedalScore = 2000;
    [SerializeField]
    int SilverMedalScore = 1000;
    [SerializeField]
    int BronzeMedalScore = 500;

    [SerializeField]
    Sprite GoldMedal;
    [SerializeField]
    Sprite SilverMedal;
    [SerializeField]
    Sprite BronzeMedal;

    Image[] ScoreboardMedals;

    Image FinalScoreMedal;

    [SerializeField]
    bool resetSavedScores;

    Text currentScoreText;
    Text topScoreText_0;
    Text topScoreText_1;
    Text topScoreText_2;
    List<int> highScores = new List<int>();

    void OnEnable()
    {
        PlayerController.OnGameOver += SetHighScoreText;
        PlayerController.OnGameOver += SetMedals;
    }
    
    void OnDisable()
    {
        PlayerController.OnGameOver -= SetHighScoreText;
        PlayerController.OnGameOver -= SetMedals;
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (resetSavedScores)
        {
            PlayerPrefs.SetInt("highScore_0", 0);
            PlayerPrefs.SetInt("highScore_1", 0);
            PlayerPrefs.SetInt("highScore_2", 0);
        }

        currentScoreText = GameObject.Find("HighScore").GetComponent<Text>();
        topScoreText_0 = GameObject.Find("TopScore_0").GetComponent<Text>();
        topScoreText_1 = GameObject.Find("TopScore_1").GetComponent<Text>();
        topScoreText_2 = GameObject.Find("TopScore_2").GetComponent<Text>();
        
        ScoreboardMedals = new Image[3];
        ScoreboardMedals[0] = GameObject.Find("ScoreMedal_1").GetComponent<Image>();
        ScoreboardMedals[1] = GameObject.Find("ScoreMedal_2").GetComponent<Image>();
        ScoreboardMedals[2] = GameObject.Find("ScoreMedal_3").GetComponent<Image>();

        FinalScoreMedal = GameObject.Find("MedalAchieved").GetComponent<Image>();
        FinalScoreMedal.enabled = false;

        highScores.Insert(0, PlayerPrefs.GetInt("highScore_0"));
        highScores.Insert(1, PlayerPrefs.GetInt("highScore_1"));
        highScores.Insert(2, PlayerPrefs.GetInt("highScore_2"));

        SetTopScoresText();
        SetHighScoreText();
        SetMedals();
    }

    public void FinalMedal(int finalScore) 
    {
        if (finalScore > GoldMedalScore)
        {
            FinalScoreMedal.enabled = true;
            FinalScoreMedal.sprite = GoldMedal;
        }
        else if (finalScore > SilverMedalScore)
        {
            FinalScoreMedal.enabled = true;
            FinalScoreMedal.sprite = SilverMedal;
        }
        else if (finalScore > BronzeMedalScore)
        {
            FinalScoreMedal.enabled = true;
            FinalScoreMedal.sprite = BronzeMedal;
        }
        else
        {
            //ScoreboardMedals[i].sprite = null;
            FinalScoreMedal.enabled = false;
        }
    }

    void SetMedals() 
    {
        for (int i = 0; i < highScores.Count; i++)
        {
            if (highScores[i] > GoldMedalScore)
            {
                ScoreboardMedals[i].enabled = true;
                ScoreboardMedals[i].sprite = GoldMedal;
            }
            else if (highScores[i] > SilverMedalScore)
            {
                ScoreboardMedals[i].enabled = true;
                ScoreboardMedals[i].sprite = SilverMedal;
            }
            else if (highScores[i] > BronzeMedalScore)
            {
                ScoreboardMedals[i].enabled = true;
                ScoreboardMedals[i].sprite = BronzeMedal;
            }
            else 
            {
                ScoreboardMedals[i].sprite = null;
                ScoreboardMedals[i].enabled = false;
            }
        }
    }

    // Update is called once per frame
    public bool NewHighScore(int lastScore)
    {
        for (int i = 0; i < highScores.Count; i++)
        {
            if(lastScore > highScores[i])
            {
                highScores.Insert(i, lastScore);
                highScores.RemoveAt(highScores.Count - 1);
                SetTopScoresText();
                SaveData();
                SetMedals();

                if (i == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    void SaveData()
    {
        PlayerPrefs.SetInt("highScore_0", highScores[0]);
        PlayerPrefs.SetInt("highScore_1", highScores[1]);
        PlayerPrefs.SetInt("highScore_2", highScores[2]);
    }

    void SetHighScoreText()
    {
        currentScoreText.text = "" + highScores[0];
    }

    void SetTopScoresText()
    {
        topScoreText_0.text = "1. " + highScores[0];
        topScoreText_1.text = "2. " + highScores[1];
        topScoreText_2.text = "3. " + highScores[2];
    }
}
