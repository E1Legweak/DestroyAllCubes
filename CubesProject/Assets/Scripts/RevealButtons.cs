﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevealButtons : MonoBehaviour
{
    [SerializeField]
    GameObject[] buttons;
    [SerializeField]
    float pauseTime = 2f;
    
    void OnEnable()
    {
        ButtonsReveal();
    }

    void OnDisable()
    {
        HideButtons();
    }

    void ButtonsReveal()
    {
        HideButtons();
        Invoke("ShowButtons", pauseTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ShowButtons()
    {
        foreach (GameObject item in buttons)
        {
            item.SetActive(true);
        }
    }

    void HideButtons()
    {
        foreach (GameObject item in buttons)
        {
            item.SetActive(false);
        }
    }
}
