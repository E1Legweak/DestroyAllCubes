﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMesh : MonoBehaviour
{
    public Vector3 rotation = Vector3.zero;
    bool play = true;

    void OnEnable()
    {
        GameStateMachine.OnStartScreen += StartMotion;
        PlayerController.OnGameOver += StopMotion;
        PlayerController.OnLostLife += StopMotion;
        PlayerController.OnResetAfterDeath += StartMotion;
    }

    void OnDisable()
    {
        GameStateMachine.OnStartScreen -= StartMotion;
        PlayerController.OnGameOver -= StopMotion;
        PlayerController.OnLostLife -= StopMotion;
        PlayerController.OnResetAfterDeath -= StartMotion;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (play)
        {
            if (TimeManager.timeSpeed > 0)
            {
                Vector3 rot = rotation * Time.deltaTime;
                transform.Rotate(rotation, Space.Self);
            }
        }
    }

    void StopMotion() 
    {
        play = false;
    }

    void StartMotion()
    {
        play = true;
    }
}
