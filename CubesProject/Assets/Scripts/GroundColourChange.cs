﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundColourChange : MonoBehaviour
{
    Material mat;
    [SerializeField]
    Color startColour;
    [SerializeField]
    Color targetColour;

    void OnEnable()
    {
        ScoreManager.OnEnterFrenzy += ToTargetColour;
        PlayerController.OnEndFrenzy += ToStartColour;
        PlayerController.OnGameOver += ToStartColour;
    }

    void OnDisable()
    {
        ScoreManager.OnEnterFrenzy -= ToTargetColour;
        PlayerController.OnEndFrenzy -= ToStartColour;
        PlayerController.OnGameOver -= ToStartColour;
    }

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ToTargetColour()
    {
        //Debug.Log("Shit Happens 1");
        mat.SetColor("_BaseColor", targetColour);
    }

    void ToStartColour()
    {
        //Debug.Log("Shit Happens 2");
        mat.SetColor("_BaseColor", startColour);
    }
}
