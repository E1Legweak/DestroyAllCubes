﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    bool slowDown = false;

    public static float timeSpeed = 1;

    void OnEnable()
    {
        PlayerCollisions.OnMultiplyEnemyKilled += StartSlowMo;
        ScoreManager.OnEnterFrenzy += StartSlowMo;
        PlayerController.OnEndFrenzy += StartSlowMo;
        PlayerLifeCounter.OnExtraLife += StartSlowMo;
        PlayerController.OnGameOver += ResetTimeSpeed;
    }

    void OnDisable()
    {
        PlayerCollisions.OnMultiplyEnemyKilled -= StartSlowMo;
        ScoreManager.OnEnterFrenzy -= StartSlowMo;
        PlayerController.OnEndFrenzy -= StartSlowMo;
        PlayerLifeCounter.OnExtraLife -= StartSlowMo;
        PlayerController.OnGameOver -= ResetTimeSpeed;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void TimeUpdate()
    {
        SlowMo();
    }

    void SlowMo()
    {
        if (slowDown)
        {
            CancelInvoke("ResetTimeSpeed");
            timeSpeed = 0;
            Invoke("ResetTimeSpeed", 0.2f);
            slowDown = false;
        }
    }

    void ResetTimeSpeed()
    {
        timeSpeed = 1;
    }

    void LerpTime()
    {
        if (Time.timeScale < 0.99f)
        {
            float targetTimeScale = Mathf.Lerp(Time.timeScale, 1, 0.02f);
            Time.timeScale = targetTimeScale;
        }
        else
        {
            Time.timeScale = 1;
            slowDown = false;
        }
    }

    void StartSlowMo()
    {
        slowDown = true;
        //Time.timeScale = 0.3f;
    }
}
