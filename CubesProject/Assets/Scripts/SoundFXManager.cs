﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXManager : MonoBehaviour
{
    [SerializeField]
    AudioSource r_EnemyDeath;
    [SerializeField]
    AudioSource r_PlayerTeleport;
    [SerializeField]
    AudioSource r_LowEnergy;
    [SerializeField]
    AudioSource r_HazardDeath;
    [SerializeField]
    AudioSource r_PlayerDeath;
    [SerializeField]
    AudioSource r_ExtraLife;
    [SerializeField]
    AudioSource r_EnterFrenzy;
    [SerializeField]
    AudioSource r_HazardTeleport;
    [SerializeField]
    AudioSource r_EnemyTeleport;
    [SerializeField]
    AudioSource r_ButtonPress;
    [SerializeField]
    AudioSource r_AttackMove;
    [SerializeField]
    AudioSource r_RegularMove;
    [SerializeField]
    AudioSource r_MineBlip;

    void OnEnable()
    {
        GameStateMachine.OnEnterGame += PlayerTeleport;
        PlayerController.OnResetAfterDeath += PlayerTeleport;
        PlayerController.OnGameOver += PlayerDeath;
        PlayerController.OnLostLife += PlayerDeath;
        ScoreManager.OnEnterFrenzy += EnteringFrenzy;
        GameStateMachine.OnButtonPress += ButtonPress;
        PlayerLifeCounter.OnExtraLife += ExtraLife;
        PlayerController.OnEnergyLow += LowEnergy;
    }


    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= PlayerTeleport;
        PlayerController.OnResetAfterDeath -= PlayerTeleport;
        PlayerController.OnGameOver -= PlayerDeath;
        PlayerController.OnLostLife -= PlayerDeath;
        ScoreManager.OnEnterFrenzy -= EnteringFrenzy;
        GameStateMachine.OnButtonPress -= ButtonPress;
        PlayerLifeCounter.OnExtraLife -= ExtraLife;
        PlayerController.OnEnergyLow -= LowEnergy;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void EnemyDeath()
    {
        r_EnemyDeath.Stop();
        r_EnemyDeath.pitch = Random.Range(0.8f, 1.2f);
        r_EnemyDeath.Play();
    }

    void PlayerTeleport()
    {
        r_PlayerTeleport.Stop();
        r_PlayerTeleport.Play();
    }

    public void HazardDeath()
    {
        r_HazardDeath.Stop();
        r_HazardDeath.pitch = Random.Range(0.8f, 1.2f);
        r_HazardDeath.Play();
    }

    void PlayerDeath()
    {
        r_PlayerDeath.Stop();
        r_PlayerDeath.Play();
    }

    void EnteringFrenzy()
    {
        r_EnterFrenzy.Stop();
        r_EnterFrenzy.Play();
    }

    public void HazardTeleport()
    {
        r_HazardTeleport.Stop();
        r_HazardTeleport.Play();
    }

    void ButtonPress()
    {
        r_ButtonPress.Stop();
        r_ButtonPress.Play();
    }

    void ExtraLife()
    {
        r_ExtraLife.Stop();
        r_ExtraLife.Play();
    }

    void LowEnergy()
    {
        r_LowEnergy.Stop();
        r_LowEnergy.Play();
    }

    public void EnemyTeleport()
    {
        r_EnemyTeleport.Stop();
        r_EnemyTeleport.Play();
    }

    public void AttackMove()
    {
        r_AttackMove.Stop();
        r_AttackMove.pitch = Random.Range(0.9f, 1.1f);
        r_AttackMove.Play();
    }

    public void RegularMove()
    {
        r_RegularMove.Stop();
        r_RegularMove.pitch = Random.Range(0.9f, 1.1f);
        r_RegularMove.Play();
    }
    public void MineBeep() 
    {
        r_MineBlip.Stop();
        r_MineBlip.Play();
    }
}
