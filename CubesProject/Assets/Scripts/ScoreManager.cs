﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    int EnemyScoreValue = 10;

    [SerializeField]
    int FrenzyScoreInterval = 100;

    public static int currentScore = 0;
    public static int killMultiplier = 1;

    Text currentScoreText;
    Text finalScoreText;
    
    int frenzyCount = 1;
    SaveDataManager _sDManager;

    public delegate void EnterFrenzy();
    public static event EnterFrenzy OnEnterFrenzy;

    void OnEnable()
    {
        PlayerController.OnGameOver += ResetScores;
        PlayerCollisions.OnEnemyKilled += AddCurrentScore;
        PlayerCollisions.OnMultiplyEnemyKilled += AddCurrentScore;
        FingerCollisions.OnFingerPress += ResetKillMulti;
        PlayerController.OnMoveEnd += ResetKillMulti;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= ResetScores;
        PlayerCollisions.OnEnemyKilled -= AddCurrentScore;
        PlayerCollisions.OnMultiplyEnemyKilled -= AddCurrentScore;
        FingerCollisions.OnFingerPress -= ResetKillMulti;
        PlayerController.OnMoveEnd -= ResetKillMulti;
    }

    // Start is called before the first frame update
    void Awake()
    {
        currentScoreText = GameObject.Find("CurrentScore").GetComponent<Text>();
        finalScoreText = GameObject.Find("FinalScore").GetComponent<Text>();
        _sDManager = GetComponent<SaveDataManager>();
        currentScoreText.text = "" + 0;
    }

    void AddCurrentScore()
    {
        currentScore = currentScore + (EnemyScoreValue * killMultiplier);
        currentScoreText.text = "" + currentScore;
        killMultiplier++;
        CheckFrenzy();
    }

    void CheckFrenzy()
    {
        if ((currentScore / 20) > frenzyCount)
        {
            frenzyCount += 50;
            OnEnterFrenzy?. Invoke();
        }
    }

    void ResetKillMulti()
    {
        killMultiplier = 1;
    }

    void AttackEnd()
    {
        
    }

    void ResetScores()
    {
        if (_sDManager.NewHighScore(currentScore))
        {
            finalScoreText.text = "New High Score!: " + currentScore;
        }
        else
        {
            finalScoreText.text = "Score: " + currentScore;
        }

        _sDManager.FinalMedal(currentScore);

        currentScore = 0;
        frenzyCount = 1;
        killMultiplier = 1;
        currentScoreText.text = "" + currentScore;
    }
}
