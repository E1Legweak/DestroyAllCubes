﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolEnemyController : EnemyController
{
    //[SerializeField]
    public float patrolSpeed = 8f;

    Collider _pCol;
    Renderer _pRend;
    Vector3 _lastFramePos = Vector3.zero;

    Vector3 targetLoc = Vector3.zero;
    Vector3 moveDelta = Vector3.zero;

    SoundFXManager soundFX;

    public enum PatrolEnemyType {vertical, horizontal};
    public PatrolEnemyType currentPatrolEnemyType = PatrolEnemyType.horizontal;

    enum PatrolEnemyState {prepAttack, attacking, penned, stopped};
    PatrolEnemyState currentPatrolEnemyState = PatrolEnemyState.penned;

    public override bool IsPenned()
    {
        if (currentPatrolEnemyState == PatrolEnemyState.penned)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnEnable()
    {
        PlayerController.OnGameOver += Stopped;
        PlayerController.OnLostLife += Stopped;
        GameStateMachine.OnStartScreen += Killed;
        PlayerController.OnResetAfterDeath += Killed;
    }

    void OnDisable()
    {
        PlayerController.OnGameOver -= Stopped;
        PlayerController.OnLostLife -= Stopped;
        GameStateMachine.OnStartScreen -= Killed;
        PlayerController.OnResetAfterDeath -= Killed;
    }

    // Start is called before the first frame update
    void Start()
    {
        //currentPatrolEnemyType = PatrolEnemyType.horizontal;
        //SetTagetLoc();
        soundFX = GameObject.Find("Sound").GetComponent<SoundFXManager>();
        _pCol = GetComponent<Collider>();
        _pCol.enabled = false;
        _pRend = GetComponent<Renderer>();
        _pRend.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //EnemyUpdate();
    }

    public override void EnemyUpdate()
    {
        switch (currentPatrolEnemyState)
        {
            case PatrolEnemyState.attacking:
                Move();
                break;

            case PatrolEnemyState.stopped:
                break;

            case PatrolEnemyState.penned:
                break;

            default:
                Debug.LogError("Invalid Enemy State");
                break;
        }
    }

    void Move()
    {
        //Debug.Log(targetLoc);
        if (Vector3.Distance(transform.position, targetLoc) > moveDelta.magnitude + 0.2f)
        {

        }
        else
        {
            transform.position = targetLoc;
            FlipTarget();
        }
        transform.position = transform.position + (transform.forward * patrolSpeed * Time.deltaTime * TimeManager.timeSpeed);
        moveDelta = _lastFramePos - transform.position;
        _lastFramePos = transform.position;
    }

    void FlipTarget() 
    {
        if (currentPatrolEnemyType == PatrolEnemyType.vertical)
        {
            targetLoc = new Vector3(targetLoc.x, targetLoc.y, -targetLoc.z);
            transform.LookAt(targetLoc);
        }
        else 
        {
            targetLoc = new Vector3(-targetLoc.x, targetLoc.y, targetLoc.z);
            transform.LookAt(targetLoc);
        }
    }

    void SetTagetLoc() 
    {
        if (currentPatrolEnemyType == PatrolEnemyType.vertical)
        {
            targetLoc = new Vector3(transform.position.x, transform.position.y, -transform.position.z);
        }
        else
        {
            targetLoc = new Vector3(-transform.position.x, transform.position.y, transform.position.z);
        }
    }

    public override bool IsSpawnable()
    {
        if (currentPatrolEnemyState == PatrolEnemyState.penned)
        {
            return true;
        }
        return false;
    }

    public override void Killed()
    {
        //Debug.Log("Death!!!!!");
        CancelInvoke("StartMotion");
        currentPatrolEnemyState = PatrolEnemyState.penned;
        
        transform.position = new Vector3(0, 0, 12);
        moveDelta = Vector3.zero;
        _lastFramePos = transform.position;
        _pCol.enabled = false;
        //_pRend.enabled = false;
        Invoke("Hide", Time.deltaTime);
    }

    void Hide() 
    {
        _pRend.enabled = false;
    }

    public override void Destroyed()
    {
        Killed();
    }

    public void PatrolSpawn(Vector3 spawnPos)
    {
        soundFX.EnemyTeleport();
        currentPatrolEnemyState = PatrolEnemyState.stopped;
        transform.position = spawnPos;
        SetTagetLoc();
        transform.LookAt(targetLoc);
        Invoke("StartMotion", 0.5f);
        //Debug.Log("Shitttt");
        //BroadcastMessage("Spawned");
    }

    void StartMotion()
    {
        //SetTagetLoc();
        //transform.LookAt(targetLoc);
        _pCol.enabled = true;
        _pRend.enabled = true;
        _lastFramePos = transform.position;
        moveDelta = _lastFramePos - transform.position;
        currentPatrolEnemyState = PatrolEnemyState.attacking;
    }

    public override void Stopped()
    {
        //transform.position = _lastFramePos;
        //_lastFramePos = transform.position;
        currentPatrolEnemyState = PatrolEnemyState.stopped;
        CancelInvoke("StartMotion");
    }
}
