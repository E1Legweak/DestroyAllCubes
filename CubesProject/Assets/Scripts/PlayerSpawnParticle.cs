﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnParticle : MonoBehaviour
{
    ParticleSystem pSystem;

    void OnEnable()
    {
        GameStateMachine.OnEnterGame += PlayParticle;
        PlayerController.OnResetAfterDeath += PlayParticle;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= PlayParticle;
        PlayerController.OnResetAfterDeath -= PlayParticle;
    }

    // Start is called before the first frame update
    void Start()
    {
        pSystem = GetComponent<ParticleSystem>();
    }

    void PlayParticle()
    {
        pSystem.Play();
    }

}
