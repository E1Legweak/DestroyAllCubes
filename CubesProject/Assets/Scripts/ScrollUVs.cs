﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollUVs : MonoBehaviour
{
    MeshRenderer rend;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    Vector2 uvOffset = Vector2.zero;
    bool play = true;

    void OnEnable()
    {
        GameStateMachine.OnStartScreen += StartMotion;
        PlayerController.OnGameOver += StopMotion;
        PlayerController.OnLostLife += StopMotion;
        PlayerController.OnResetAfterDeath += StartMotion;
    }
    void OnDisable()
    {
        GameStateMachine.OnStartScreen -= StartMotion;
        PlayerController.OnGameOver -= StopMotion;
        PlayerController.OnLostLife -= StopMotion;
        PlayerController.OnResetAfterDeath -= StartMotion;
    }

    private void Awake()
    {
        rend = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (play)
        {
            uvOffset += (uvAnimationRate * Time.deltaTime);
            if (rend.enabled)
            {
                rend.material.SetTextureOffset("_BaseMap", uvOffset);
            }
        }
    }
    void StopMotion()
    {
        play = false;
    }

    void StartMotion()
    {
        play = true;
    }
}
