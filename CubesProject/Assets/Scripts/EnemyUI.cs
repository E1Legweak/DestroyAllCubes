﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using TMPro;
using UnityEngine.UI;

public class EnemyUI : MonoBehaviour
{
    [SerializeField]
    GameObject scoreUI;

    [SerializeField]
    GameObject radarUI;

    GameObject _score;
    Text _scoreText;
    RectTransform _scoreTransform;
    Color _startColour;
    Color _targetColour = new Color(1, 1, 1, 0);

    GameObject _radar;
    SpriteRenderer _radarImage;
    //RectTransform _radarTransform;
    Transform _radarTransform;

    RectTransform canvas;
    Renderer _rend;

    Transform _playerTransform;

    bool _onScreen;
    bool _scoreShown;

    float screenX;
    float screenZ;

    void OnEnable()
    {
        PlayerController.OnGameOver += SetRadarOff;
        PlayerController.OnGameOver += SetScoreTextOff;
        PlayerController.OnLostLife += SetRadarOff;
        PlayerController.OnLostLife += SetScoreTextOff;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= SetRadarOff;
        PlayerController.OnGameOver -= SetScoreTextOff;
        PlayerController.OnLostLife -= SetRadarOff;
        PlayerController.OnLostLife -= SetScoreTextOff;
    }

    // Start is called before the first frame update
    void Awake()
    {
        canvas = GameObject.Find("PlayScreen").GetComponent<RectTransform>();
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        _rend = GetComponentInChildren<Renderer>();

        _score = Instantiate(scoreUI);
        _score.transform.SetParent(canvas.transform);
        _scoreText = _score.GetComponent<Text>();
        _scoreTransform = _score.GetComponent<RectTransform>();
        _startColour = _scoreText.color;

        _radar = Instantiate(radarUI);
        //_radar.transform.SetParent(canvas.transform);
        _radar.transform.position = new Vector3(-20,0,0);
        _radarImage = _radar.GetComponent<SpriteRenderer>();
        _radarTransform = _radar.GetComponent<Transform>();

        SetRadarOff();
        SetScoreTextOff();
        GetScreenSize();
    }

    void SetRadarOff()
    {
        _radarImage.enabled = false;
        _onScreen = true;
    }

    void SetRadarOn()
    {
        _radarImage.enabled = true;
        _onScreen = false;
    }

    void SetScoreTextOn()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(_playerTransform.position);  
        _scoreTransform.position = pos;

        _onScreen = false;
        _scoreText.enabled = true;
        _scoreText.text = (ScoreManager.killMultiplier * 10).ToString();
        _scoreText.fontSize = Mathf.RoundToInt(20f * (ScoreManager.killMultiplier * 0.5f));
    }

    void SetScoreTextOff()
    {
        _scoreText.enabled = false;
        _scoreText.text = null;
        _scoreText.color = _startColour;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_onScreen)
        {
            CheckVisibility();
            RadarPosition();
        }
        if (_scoreShown)
        {
            ScoreDrift();
            ScoreFade();
        }
    }

    void CheckVisibility()
    {
        if (_rend.isVisible)
        {
            SetRadarOff();
        }
    }

    public void Killed()
    {
        //Debug.Log("Ppppoppopop");
        SetScoreTextOn();
        _scoreShown = true;
        Invoke("ResetScore", 1);
    }

    private void ResetScore()
    {
        SetScoreTextOff();
        _scoreShown = false;
    }

    public void Spawned()
    {
        SetRadarOn();
    }

    void ScoreDrift()
    {
        _scoreTransform.position = _scoreTransform.position + new Vector3(0, 10 * Time.deltaTime, 0);
    }

    void ScoreFade()
    {
        _scoreText.color = Color.Lerp(_scoreText.color, _targetColour, 10 * Time.deltaTime);
    }

    void GetScreenSize()
    {
        Vector3 ScreenSize = new Vector3(Screen.width, Screen.height, 0);
        Vector3 ScreenEdge = Camera.main.ScreenToWorldPoint(ScreenSize);
        screenX = ScreenEdge.x;
        screenZ = ScreenEdge.z;
    }

    //TODO Make scaleable to different devices. Get screen width and height and convert back to world space.
    void RadarPosition()
    {
        //Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 pos = transform.position;
        Vector3 posClampled = new Vector3(Mathf.Clamp(pos.x, -screenX, screenX), 1f, Mathf.Clamp(pos.z, -screenZ, screenZ));

        if (pos.z > screenZ || pos.z < -screenZ)
        {
            if (pos.x < screenX || pos.x > -screenX)
            {
                if (pos.z > 0)
                {
                    _radarTransform.localRotation = Quaternion.Euler(new Vector3(90, 180, 0));
                }
                else
                {
                    _radarTransform.localRotation = Quaternion.Euler(new Vector3(90, 0, 0));
                }
            }
        }
        else
        {
            if (pos.x > 0)
            {
                _radarTransform.localRotation = Quaternion.Euler(new Vector3(90, 270, 0));
            }
            else
            {
                _radarTransform.localRotation = Quaternion.Euler(new Vector3(90, 90, 0));
            }
        }
        _radarTransform.position = posClampled;
    }
}
