﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    [SerializeField]
    AudioMixerSnapshot r_Normal;
    [SerializeField]
    AudioMixerSnapshot r_Dead;

    //[SerializeField]
    //AudioClip r_MenuMusic;
    [SerializeField]
    AudioSource[] r_PlayMusics;

    [SerializeField]
    AudioSource r_MenuMusic;

    bool waitingForPlay = true;
    float timeMusicPlayed = 0;
    int musicClipIndex = 0;
    bool isPlaying = false;
    int numberOfPlays = 0;


    void OnEnable()
    {
        PlayerController.OnGameOver += StopMusic;
        GameStateMachine.OnEnterGame += StartPlayMusic;
        GameStateMachine.OnStartScreen += StartMenuMusic;
        //PlayerController.OnGameOver += PauseMusic;
        PlayerController.OnLostLife += PauseMusic;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= StopMusic;
        GameStateMachine.OnEnterGame -= StartPlayMusic;
        GameStateMachine.OnStartScreen -= StartMenuMusic;
        //PlayerController.OnGameOver -= PauseMusic;
        PlayerController.OnLostLife -= PauseMusic;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartMenuMusic();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            MusicChange();
        }
    }

    void StartMenuMusic()
    {
        //Debug.Log("MenuMusic");
        if (waitingForPlay == true)
        {
            SnapShotChange(r_Normal, 0);
            r_MenuMusic.Play();

            waitingForPlay = false;
        }
    }

    void StartPlayMusic()
    {
        //r_MainMusic.clip = r_PlayMusics[0];
        r_PlayMusics[0].Play();
        r_MenuMusic.Stop();
        isPlaying = true;
        timeMusicPlayed = Time.time + r_PlayMusics[0].clip.length;
        musicClipIndex = 0;
        waitingForPlay = true;
    }

    void StopMusic()
    {
        CancelInvoke();
        Invoke("Stop", 1f);
        SnapShotChange(r_Dead, 0.2f);
        isPlaying = false;
    }

    void Stop()
    {
        r_PlayMusics[musicClipIndex].Stop();
        /*foreach (AudioSource sound in r_PlayMusics) 
        {
            sound.Stop();
        }*/
    }

    void PauseMusic()
    {
        CancelInvoke();
        SnapShotChange(r_Dead, 0.2f);
        Invoke("HitPause", 1f);
    }

    void HitPause()
    {
        CancelInvoke();
        r_PlayMusics[musicClipIndex].Pause();
        timeMusicPlayed = timeMusicPlayed + 1;
        Invoke("RestartMusic", 1f);
    }


    void SnapShotChange(AudioMixerSnapshot target, float t)
    {
        target.TransitionTo(t);
    }

    void RestartMusic()
    {
        r_PlayMusics[musicClipIndex].UnPause();
        SnapShotChange(r_Normal, 0.2f);
    }

    void MusicChange()
    {
        //Debug.Log("MusicPlay");
        //Debug.Log(r_PlayMusics[musicClipIndex].length);
        if (Time.time >= timeMusicPlayed + Time.deltaTime)
        {
            r_PlayMusics[musicClipIndex].Stop();
            if (musicClipIndex < r_PlayMusics.Length - 1)
            {
                
                musicClipIndex++;
            }
            else
            {
                musicClipIndex = 0;
                if (numberOfPlays < 2)
                {
                    numberOfPlays++;
                }
                else
                {
                    musicClipIndex = 0;
                    numberOfPlays = 0;
                }
            }
            //r_MenuMusic.Stop();
            r_PlayMusics[musicClipIndex].Play(); ;
            //r_MenuMusic.Play();
            timeMusicPlayed = Time.time + r_PlayMusics[musicClipIndex].clip.length;
        }
    }
}
