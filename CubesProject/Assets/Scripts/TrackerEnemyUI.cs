﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrackerEnemyUI : MonoBehaviour
{
    [SerializeField]
    GameObject scoreUI;

    GameObject _score;
    Text _scoreText;
    RectTransform _scoreTransform;
    Color _startColour;
    Color _targetColour = new Color(1, 1, 1, 0);

    RectTransform canvas;

    Transform _playerTransform;

    bool _scoreShown;


    void OnEnable()
    {
        PlayerController.OnGameOver += SetScoreTextOff;
        PlayerController.OnLostLife += SetScoreTextOff;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= SetScoreTextOff;
        PlayerController.OnLostLife -= SetScoreTextOff;
    }

    // Start is called before the first frame update
    void Awake()
    {
        canvas = GameObject.Find("PlayScreen").GetComponent<RectTransform>();
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        _score = Instantiate(scoreUI);
        _score.transform.SetParent(canvas.transform);
        _scoreText = _score.GetComponent<Text>();
        _scoreTransform = _score.GetComponent<RectTransform>();
        _startColour = _scoreText.color;

        SetScoreTextOff();
    }

    void SetScoreTextOn()
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(_playerTransform.position);
        _scoreTransform.position = pos;

        _scoreText.enabled = true;
        _scoreText.text = (ScoreManager.killMultiplier * 10).ToString();
        _scoreText.fontSize = Mathf.RoundToInt(20f * (ScoreManager.killMultiplier * 0.5f));
    }

    void SetScoreTextOff()
    {
        _scoreText.enabled = false;
        _scoreText.text = null;
        _scoreText.color = _startColour;
    }

    // Update is called once per frame
    void Update()
    {
        if (_scoreShown)
        {
            ScoreDrift();
            ScoreFade();
        }
    }

    public void Killed()
    {
        SetScoreTextOn();
        _scoreShown = true;
        Invoke("ResetScore", 1);
    }

    private void ResetScore()
    {
        SetScoreTextOff();
        _scoreShown = false;
    }

    void ScoreDrift()
    {
        _scoreTransform.position = _scoreTransform.position + new Vector3(0, 10 * Time.deltaTime, 0);
    }

    void ScoreFade()
    {
        _scoreText.color = Color.Lerp(_scoreText.color, _targetColour, 10 * Time.deltaTime);
    }

}
