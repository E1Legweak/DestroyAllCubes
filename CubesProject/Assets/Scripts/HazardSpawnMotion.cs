﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSpawnMotion : MonoBehaviour
{
    public GameObject hazardPrefab;
    public GameObject hazardSpawnParticlePrefab;
    public int numberOfHazards = 1;
    public HazardController[] hazards;
    ParticleSystem hazardSpawnParticle;
    Transform _player;
    SoundFXManager soundFX;
    //Renderer rend;

    float speed = 5;
    float nextDirChange;
    float rotateAmount;
    float nextSpawn;
    float spawnInterval = 0.12f;
    Vector3 SpawnLoc = Vector3.zero;

    bool randomSpawn = false;

    bool isActive;

    public bool IsActive
    {
        get
        {
            return isActive;
        }
        set
        {
            isActive = value;
        }
    }

    void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        soundFX = GameObject.Find("Sound").GetComponent<SoundFXManager>();
        //rend = GetComponentInChildren<Renderer>();
        Physics.IgnoreLayerCollision(10, 10);
        CreateHazards();
    }

    void OnEnable()
    {
        GameStateMachine.OnStartScreen += StopParticle;
        PlayerController.OnResetAfterDeath += StopParticle;
        PlayerController.OnGameOver += StopSpawner;
        PlayerController.OnGameOver += Pause;
        PlayerController.OnLostLife += StopSpawner;
        PlayerController.OnLostLife += Pause;
        PlayerController.OnGameOver += StopInvoke;
        PlayerController.OnLostLife += StopInvoke;
    }


    void OnDisable()
    {
        GameStateMachine.OnStartScreen -= StopParticle;
        PlayerController.OnResetAfterDeath -= StopParticle;
        PlayerController.OnGameOver -= StopSpawner;
        PlayerController.OnGameOver -= Pause;
        PlayerController.OnLostLife -= StopSpawner;
        PlayerController.OnLostLife -= Pause;
        PlayerController.OnGameOver -= StopInvoke;
        PlayerController.OnLostLife -= StopInvoke;
    }

    void CreateHazards()
    {
        hazards = new HazardController[numberOfHazards];
        for (int i = 0; i < hazards.Length; i++)
        {
            hazards[i] = Instantiate(hazardPrefab).GetComponent<HazardController>();
            hazards[i].transform.position = new Vector3(0, 0, -12);
        }
        hazardSpawnParticle = Instantiate(hazardSpawnParticlePrefab).GetComponent<ParticleSystem>();
        hazardSpawnParticle.transform.position = new Vector3(-12, 2, -12);
    }

    void Pause()
    {
        hazardSpawnParticle.Pause();
    }

    void StopParticle()
    {
        hazardSpawnParticle.Clear();
        //hazardSpawnParticle.transform.position = new Vector3(-12, 2, -12);
    }

    public void StopSpawner()
    {
        isActive = false;
        Pen();
        hazardSpawnParticle.Stop();

        int dice = Random.Range(0, 2);
        if (dice >= 1)
        {
            randomSpawn = false;
        }
        else
        {
            randomSpawn = true;
        }
    }

    public void HazardSpawnUpdate()
    {
        if (isActive)
        {
            if (randomSpawn)
            {
                if (IsNextSpawn())
                {
                    transform.position = GeneratePos();
                    hazardSpawnParticle.transform.position = transform.position;
                    hazardSpawnParticle.Stop();
                    hazardSpawnParticle.Play();
                    SpawnLoc = transform.position;
                    Invoke("DropHazard", 0.4f);
                }
            }
            else
            {
                //RandomDirectionSet();
                //transform.position = transform.position + (transform.forward * speed * Time.deltaTime * TimeManager.timeSpeed);

                if (IsNextSpawn())
                {
                    RandomDirectionSet();
                    
                    transform.position += transform.forward;
                    hazardSpawnParticle.transform.position = transform.position;
                    hazardSpawnParticle.Stop();
                    hazardSpawnParticle.Play();
                    SpawnLoc = transform.position;
                    Invoke("DropHazard", 0.4f);
                }
                //Spawn();
            }
        }
    }

    void StopInvoke()
    {
        CancelInvoke();
    }

    void PlaceHazard(Vector3 loc)
    {

    }

    public void SpawnStart()
    {
        nextDirChange = Time.time + Random.Range(1, 3);
    }

    bool IsDirectionChange()
    {
        if (Time.time > nextDirChange)
        {
            nextDirChange = Time.time + Random.Range(1,3);
            return true;
        }
        return false;
    }

    void RandomDirectionSet()
    {
        if (IsDirectionChange())
        {
            int dice = Random.Range(0, 2);
            if (dice >= 1)
            {
                rotateAmount = 90;
            }
            else
            {
                rotateAmount = -90;
            }
            transform.Rotate(new Vector3(0, rotateAmount, 0));
        }
    }

    void Pen()
    {
       transform.position = new Vector3( -12, 0, -12);
    }

    Vector3 GeneratePos()
    {
        //Debug.Log("Running!!!!");
        Vector3 pos = new Vector3(Random.Range(-8, 8) + 0.5f, 0, Random.Range(-5, 5) + 0.5f);

        while ((pos.x > _player.position.x - 0.5f && pos.x < _player.position.x + 0.5f) && (pos.z > _player.position.z - 0.5f && pos.z < _player.position.z + 0.5f))
        {
            pos = new Vector3(Random.Range(-8, 8), 0, Random.Range(-5, 5));
        }
        return pos;
    }

    bool IsNextSpawn()
    {
        if (TimeManager.timeSpeed == 0)
        {
            nextSpawn = nextSpawn + Time.deltaTime;
            return false;
        }
        if (Time.time > nextSpawn)
        {
            //Debug.Log("Spawn started");
            nextSpawn = Time.time + spawnInterval;
            return true;
        }
        return false;
    }

    void Spawn()
    {
        if (IsNextSpawn())
        {
            hazardSpawnParticle.transform.position = transform.position;
            hazardSpawnParticle.Stop();
            hazardSpawnParticle.Play();
            SpawnLoc = transform.position;
            Invoke("DropHazard", 0.4f);
        }
    }

    void DropHazard()
    {
        for (int i = 0; i < hazards.Length; i++)
        {
            if (hazards[i].IsSpawnable())
            {
                hazards[i].Spawn(SpawnLoc);
                soundFX.HazardTeleport();
                if (i == hazards.Length - 1)
                {
                    StopSpawner();
                }
                break;
            }
        }
    }
}
