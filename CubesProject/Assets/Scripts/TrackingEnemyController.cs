﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingEnemyController : EnemyController
{
    public GameObject laser;

    public float pauseTime = 1.5f;

    //[SerializeField]
    public float turnSpeed = 1f;

    //[SerializeField]
    public float trackerSpeed = 3.5f;

    public AudioSource tickSound;
    public AudioSource attackSound;

    GameObject _tPlayer;
    Transform _tVisTrans;

    Collider _tCol;
    Renderer _tRend;
    Vector3 _lastFramePos = Vector3.zero;

    Vector3 playerTargetLoc = Vector3.zero;
    Vector3 moveDelta = Vector3.zero;

    SoundFXManager soundFX;
    bool tickPaused;

    enum TrackerEnemyState { preTracking, tracking, prepAttack, attacking, penned, stopped};
    TrackerEnemyState currentTrackerEnemyState = TrackerEnemyState.penned;

    public override bool IsPenned()
    {
        if (currentTrackerEnemyState == TrackerEnemyState.penned)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnEnable()
    {
        PlayerController.OnGameOver += Stopped;
        PlayerController.OnLostLife += Stopped;
        GameStateMachine.OnStartScreen += Killed;
        PlayerController.OnResetAfterDeath += Killed;
    }

    void OnDisable()
    {
        PlayerController.OnGameOver -= Stopped;
        PlayerController.OnLostLife -= Stopped;
        GameStateMachine.OnStartScreen -= Killed;
        PlayerController.OnResetAfterDeath -= Killed;
    }

    // Start is called before the first frame update
    void Start()
    {
        laser.SetActive(false);
        soundFX = GameObject.Find("Sound").GetComponent<SoundFXManager>();
        _tRend = GetComponent<Renderer>();
        _tCol = GetComponent<Collider>();
        _tCol.enabled = false;
        _tRend.enabled = false;
        _tPlayer = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //EnemyUpdate();
    }

    void TickPause() 
    {
        if (TimeManager.timeSpeed == 0)
        {
            tickSound.Pause();
            tickPaused = true;
        }
        else if (tickPaused) 
        {
            tickSound.UnPause();
            tickPaused = false;
        }
    }

    public override void EnemyUpdate()
    {
        switch (currentTrackerEnemyState)
        {
            case TrackerEnemyState.preTracking:
                    tickSound.Play();
                laser.SetActive(true);
                currentTrackerEnemyState = TrackerEnemyState.tracking;

                break;
            case TrackerEnemyState.tracking:
                Track();
                TickPause();
                break;
            case TrackerEnemyState.prepAttack:
                laser.SetActive(false);
                tickSound.Stop();
                attackSound.Play();
                break;
            case TrackerEnemyState.attacking:
                Move();
                break;

            case TrackerEnemyState.stopped:
                break;

            case TrackerEnemyState.penned:
                break;

            default:
                Debug.LogError("Invalid Enemy State");
                break;
        }
    }

    void Move()
    {        
        if (Vector3.Distance(transform.position, playerTargetLoc) > moveDelta.magnitude + 0.5f)
        {
            //transform.position = transform.position + (transform.forward * speed * Time.deltaTime * TimeManager.timeSpeed);
        }
        else
        {
            //transform.position = playerTargetLoc;
            currentTrackerEnemyState = TrackerEnemyState.preTracking;
        }
        transform.position = transform.position + (transform.forward * trackerSpeed * Time.deltaTime * TimeManager.timeSpeed);
        moveDelta = _lastFramePos - transform.position;
        //Debug.Log(moveDelta);
        _lastFramePos = transform.position;
    }

    void Track()
    {
        //Debug.Log("Tracking");
        Vector3 lookAt = _tPlayer.transform.position - transform.position;
        if (lookAt != Vector3.zero)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAt), turnSpeed * TimeManager.timeSpeed);
        }
        if (Vector3.Angle(transform.forward, lookAt) < 2)
        {
            playerTargetLoc = _tPlayer.transform.position;
            Invoke("Attacking", pauseTime);
            currentTrackerEnemyState = TrackerEnemyState.prepAttack;
            //Debug.Log("Fire");
        }
    }

    void Attacking()
    {
        currentTrackerEnemyState = TrackerEnemyState.attacking;
    }

    public override bool IsSpawnable()
    {
        if (currentTrackerEnemyState == TrackerEnemyState.penned)
        {
            return true;
        }
        return false;
    }

    public override void Killed()
    {
        //Debug.Log("Death!!!!!");
        CancelInvoke("Attacking");
        CancelInvoke("StartMotion");
        tickSound.Stop();
        //_particle.ShowParticle();
        currentTrackerEnemyState = TrackerEnemyState.penned;
        _tCol.enabled = false;
        laser.SetActive(false);
        //_tRend.enabled = false;
        transform.position = new Vector3(0, 0, 20);
        moveDelta = Vector3.zero;
        _lastFramePos = transform.position;
        //_visTrans.localRotation = Quaternion.Euler(Vector3.zero);
        Invoke("Hide", Time.deltaTime);
    }

    void Hide()
    {
        _tRend.enabled = false;
    }

    public override void Destroyed()
    {
        Killed();
    }

    public void TrackerSpawn(Vector3 spawnPos)
    {
        soundFX.EnemyTeleport();
        transform.position = spawnPos;
        transform.LookAt(_tPlayer.transform);
        transform.Rotate(new Vector3(0, 180, 0));
        Invoke("StartMotion", 0.5f);
        //Debug.Log("Shitttt");
        //BroadcastMessage("Spawned");
        currentTrackerEnemyState = TrackerEnemyState.stopped;
    }

    void StartMotion() 
    {
        _tCol.enabled = true;
        _tRend.enabled = true;
        _lastFramePos = transform.position;
        playerTargetLoc = _tPlayer.transform.position;
        moveDelta = _lastFramePos - transform.position;
        currentTrackerEnemyState = TrackerEnemyState.preTracking;
    }

    public override void Stopped()
    {
        laser.SetActive(false);
        tickSound.Stop();
        transform.position = _lastFramePos;
        //_lastFramePos = transform.position;
        currentTrackerEnemyState = TrackerEnemyState.stopped;
        CancelInvoke("Attacking");
        CancelInvoke("StartMotion");
    }
}
