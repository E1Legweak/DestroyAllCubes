﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFrenzyColourChange : MonoBehaviour
{
    [SerializeField]
    Color baseColour;
    [SerializeField]
    Color frenzyColour;

    Material mat;
    TrailRenderer trail;

    void OnEnable()
    {
        ScoreManager.OnEnterFrenzy += ToFrenzyColour;
        PlayerController.OnEndFrenzy += ToStartColour;
        PlayerController.OnGameOver += ToStartColour;
    }

    void OnDisable()
    {
        ScoreManager.OnEnterFrenzy -= ToFrenzyColour;
        PlayerController.OnEndFrenzy -= ToStartColour;
        PlayerController.OnGameOver -= ToStartColour;
    }

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<Renderer>().material;
        trail = GetComponent<TrailRenderer>();
    }

    void ToFrenzyColour()
    {
        mat.SetColor("_BaseColor", frenzyColour);
        SetTrail(frenzyColour);
    }

    void ToStartColour()
    {
        mat.SetColor("_BaseColor", baseColour);
        SetTrail(baseColour);
    }

    void SetTrail(Color col)
    {
        //Debug.Log("What");
        trail.endColor = col;
        trail.startColor = col;
    }
}
