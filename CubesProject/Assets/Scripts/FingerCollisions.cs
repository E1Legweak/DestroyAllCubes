﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerCollisions : MonoBehaviour
{
    [SerializeField]
    float fingerWidth = 1.0f;

    [SerializeField]
    private LayerMask hitMask;

    GameObject playerAttackTarget;
    Collider[] hits;

    public delegate void FingerPress();
    public static event FingerPress OnFingerPress;

    //Gets player move target location in world space
    public GameObject GetPlayerMoveTarget(Vector3 fingerLocation)
    {
        hits = Physics.OverlapSphere(fingerLocation, fingerWidth, hitMask);
        OnFingerPress?.Invoke();

        if (hits.Length == 0)
        {
            //OnAttackStart?.Invoke();
            return null;
        }
        else if (hits.Length == 1)
        {
            //OnAttackStart?.Invoke();
            PlayerCollisions.targetEnemy = hits[0].gameObject;
            return hits[0].gameObject;
        }
        else
        {
            float distance = Vector3.Distance(transform.position, hits[0].ClosestPoint(transform.position));
            playerAttackTarget = hits[0].gameObject;
            
            for (int i = 0; i < hits.Length; i++)
            {
                float newDistance = Vector3.Distance(transform.position, hits[i].ClosestPoint(transform.position));
                if (distance < newDistance)
                {
                    playerAttackTarget = hits[i].gameObject;
                    distance = newDistance;
                }
            }
            //OnAttackStart?.Invoke();
            PlayerCollisions.targetEnemy = playerAttackTarget;
            return playerAttackTarget;
        }
    }
}
