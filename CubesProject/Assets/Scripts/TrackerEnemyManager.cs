﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackerEnemyManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject enemyParticlePrefab;
    public int numberOfEnemies = 1;
    public AnimationCurve spawnPattern;

    public TrackingEnemyController[] enemies;
    public ParticleSystem[] trackerParticles;

    Transform _player;

    float nextSpawn;
    float startTime = 5f;


    void OnEnable()
    {
        GameStateMachine.OnEnterGame += SetStartTime;
        GameStateMachine.OnEnterGame += StartSpawnTime;
        GameStateMachine.OnStartScreen += StopParticle;
        PlayerController.OnResetAfterDeath += StopParticle;
        PlayerController.OnGameOver += PauseParticle;
        PlayerController.OnLostLife += PauseParticle;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= SetStartTime;
        GameStateMachine.OnEnterGame -= StartSpawnTime;
        GameStateMachine.OnStartScreen -= StopParticle;
        PlayerController.OnResetAfterDeath -= StopParticle;
        PlayerController.OnGameOver -= PauseParticle;
        PlayerController.OnLostLife -= PauseParticle;
    }

    // Start is called before the first frame update
    void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
        Physics.IgnoreLayerCollision(8, 8);
        CreateEnemies();
        CreateParticles();
    }

    void CreateEnemies()
    {
        enemies = new TrackingEnemyController[numberOfEnemies];
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i] = Instantiate(enemyPrefab).GetComponent<TrackingEnemyController>();
            enemies[i].gameObject.transform.position = new Vector3(0, 0, -12);
        }
    }

    void CreateParticles()
    {
        trackerParticles = new ParticleSystem[numberOfEnemies];
        for (int i = 0; i < trackerParticles.Length; i++)
        {
            trackerParticles[i] = Instantiate(enemyParticlePrefab).GetComponent<ParticleSystem>();
            trackerParticles[i].gameObject.transform.position = new Vector3(0, 0, -12);
        }
    }

    void Spawn()
    {
        if (IsNextSpawn())
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i].IsSpawnable())
                {
                    Vector3 pos = GenSpawnPos();
                    //Vector3 pos = TrackerGeneratePos();
                    trackerParticles[i].gameObject.transform.position = pos;
                    trackerParticles[i].Play();
                    enemies[i].TrackerSpawn(pos);
                    break;
                }
            }
        }
    }

    Vector3 GenSpawnPos() 
    {
        Vector3 pos = new Vector3(Random.Range(-8, 8), 0, Random.Range(-5, 5));
        while ((pos.x > _player.position.x - 0.5f && pos.x < _player.position.x + 0.5f) && (pos.z > _player.position.z - 0.5f && pos.z < _player.position.z + 0.5f))
        {
            pos = new Vector3(Random.Range(-8, 8), 0, Random.Range(-5, 5));
        }
        return pos;
    }

    void PauseParticle()
    {
        foreach (ParticleSystem pSys in trackerParticles)
        {
            pSys.Pause();
        }
    }

    void UnPauseParticle()
    {
        foreach (ParticleSystem pSys in trackerParticles)
        {
            pSys.Play();
        }
    }

    void StopParticle()
    {
        foreach (ParticleSystem pSys in trackerParticles)
        {
            pSys.Stop();
            pSys.Clear();
        }
    }

    Vector3 TrackerGeneratePos()
    {
        int side = Random.Range(1, 5);
        Vector3 loc = Vector3.zero;
        switch (side)
        {
            case 1:
                loc = new Vector3(Random.Range(-12, 12), 0, Random.Range(6, 8));

                break;
            case 2:
                loc = new Vector3(Random.Range(11, 12), 0, Random.Range(-8, 8));

                break;
            case 3:
                loc = new Vector3(Random.Range(-12, 12), 0, Random.Range(-8, -6));

                break;
            case 4:
                loc = new Vector3(Random.Range(-12, -11), 0, Random.Range(-8, 8));

                break;
            default:
                //Should never be seen, but just in case
                loc = new Vector3(Random.Range(-12, -11), 0, Random.Range(-8, 8));
                break;
        }
        return loc;
    }

    void SetStartTime()
    {
        startTime = Time.time;
    }

    float GenerateNextSpawnTime()
    {
        float time = spawnPattern.Evaluate(Time.time - startTime);

        return time;
    }

    // Update is called once per frame
    public void EnemiesUpdate()
    {
        RunEnemies();
        Spawn();
    }

    void RunEnemies()
    {
        foreach (TrackingEnemyController enemy in enemies)
        {
            enemy.EnemyUpdate();
        }
    }

    bool IsNextSpawn()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + GenerateNextSpawnTime();
            return true;
        }
        return false;
    }

    void StartSpawnTime() 
    {
        nextSpawn = Time.time + GenerateNextSpawnTime();
    }
}
